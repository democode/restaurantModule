angular.module('mod.m42', []);

angular.module('mod.m42')
  .constant('MOD_restaurantModule', {
        API_URL: '',
        API_URL_DEV: ''
    })
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('m42', {
                url: '/restaurantModule',
                parent: 'layout',
                template: "<div ui-view></div>",
                data: {
                    type: 'home'
                }
            })
            .state('m42.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'mod_restaurantModule/views/menu/restaurantModule.header.html',
                   position: 1,
                   type: 'header',
                   name: "Profile",
                   module: "restaurantModule"
               }
           })
            .state('m42.dashboard', {
                url: '/dashboard',
                templateUrl: "mod_restaurantModule/views/dashboard/restaurantModule.dashboard.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Dashboard",
                    module: "restaurantModule"
                }

            })
            .state('m42.kitchen', {
                url: '/kitchen',
                templateUrl: "mod_restaurantModule/views/pages/kitchen.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Kitchen",
                    module: "restaurantModule"
                }

            })
            .state('m42.dining', {
                url: '/dining',
                templateUrl: "mod_restaurantModule/views/pages/dining.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Dining",
                    module: "restaurantModule"
                }

            })

            //FOR APP ADMIN
           .state('m42.admin', {
               url: '/admin',
               templateUrl: "mod_restaurantModule/views/admin/restaurantModule.admin.html",
               data: {
                   type: 'home',
                   menu: true,
                   admin : true,
                   disabled : false,
                   name: "Admin Page",
                   module: "restaurantModule"
               }
           })

    }])